# function external_module()
# {
#     if [ $1 == "aisco.utility.util" ]; then
#     cp libraries/aisco.utility.util.jar $modulename/
#     echo "add external module"
#     fi

#     if [ $1 == "vmj.routing.route" ]; then
#     cp libraries/vmj.routing.route.jar $modulename/
#     echo "add external module"
#     fi
# }

# function build_module()
# {
#     echo "build module $1"
#     javac -d classes --module-path $1 $(find src/$1 -name "*.java") src/$1/module-info.java
#     jar --create --file $1/$1.jar -C classes .
#     rm -r classes
#     echo "module $1 ready"
# }

# function build_module_requirement(){
#   module=$1
#   targetpath="src/$module/module-info.java"
#   req=$(cat $targetpath | grep "requires \( transitive | static \)\?"| awk '{print $2}' | cut -d';' -f 1 )

#   for reqprod in $req; do
#   echo -e "building requirement for $module: $reqprod"
#   external_module $reqprod
#   javac -d classes --module-path $product $(find src/$reqprod -name "*.java") src/$1/module-info.java 
#   jar --create --file $product/$1.jar -C classes .
#   rm -r classes
#   done
#   echo "requirement building done"
#   build_module $module
# }

# modulename=$1
# if [ -d "$1" ]; then rm -r $1; fi
# if [ -d "classes" ]; then rm -r classes; fi 
# mkdir $1
# echo " -- checking requirement -- "
# build_module_requirement $modulename
# echo "Build time: $SECONDS seconds"

function external_module()
{
    if [ $1 == "aisco.donation.pgateway" ]; then
    cp libraries/payment.page.jar $product/
    echo "add external module"
    fi

    if [ $1 == "sqlite.jdbc" ]; then
    cp libraries/sqlite-jdbc-3.30.1.jar $product/
    echo "add external module"
    fi

    if [ $1 == "gson" ]; then
    cp libraries/gson-2.8.2.jar $product/
    echo "add external module"
    fi

    if [ $1 == "aisco.utility.util" ]; then
    cp libraries/aisco.utility.util.jar $product/
    echo "add external module"
    fi

    if [ $1 == "vmj.routing.route" ]; then
    cp libraries/vmj.routing.route.jar $product/
    echo "add external module"
    fi
}

function build_product(){
  echo -e "building the product"
  javac -d cclasses  --module-path $product $(find src/$product -name "*.java") src/$product/module-info.java 
  jar --create --file $product/$product.jar -C cclasses .
  rm -r cclasses
  echo "Product is ready"
}

function build_product_requirement(){
  product=$1
  targetpath="src/$product/module-info.java"
  req=$(cat $targetpath | grep "requires"| awk '{if ($2=="transitive") print $3; else print $2;}' | cut -d';' -f 1 )
  for reqexternal in $req; do
  echo $reqexternal
  external_module $reqexternal
  done
  for reqprod in $req; do
  echo $reqprod
  javac -d cclasses --module-path $product $(find src/$reqprod -name "*.java") src/$reqprod/module-info.java
  jar --create --file $product/$reqprod.jar -C cclasses .
  rm -r cclasses
  done
  build_product
}

product=$1
echo $product
if [ -d "$1" ]; then rm -r $1; fi
if [ -d "classes" ]; then rm -r classes; fi 
mkdir $1
build_product_requirement $product
