function external_module()
{
    cp libraries/$1.jar $product/
    echo "add external module"

    jdeps_list=$(jdeps --module-path libraries libraries/$1.jar | grep "requires" | awk '{if ($2=="transitive") print $3; else if ($2=="mandated"); else print $2;}')

    echo "copying $1's dependencies"
    for jdeps_content in $jdeps_list;do
    echo "copying $jdeps_content.jar....."
    cp libraries/$jdeps_content.jar $product/
    done
    echo "done copying $1's dependencies"
}

function build_product(){
  echo -e "building the product: $mainclass"
  javac -d cclasses  --module-path $product $(find src/$product -name "*.java") src/$product/module-info.java 
  jar --create --file $product/$mainclass.jar --main-class $product.$mainclass -C cclasses .
  rm -r cclasses
  echo "Product $mainclass is ready"
}

function build_product_requirement(){
  product=$1
  targetpath="src/$product/module-info.java"
  # req=$(cat $targetpath | grep requires| awk '{print $2}' | cut -d';' -f 1 )
  req=$(cat $targetpath | grep "requires"| awk '{if ($2=="transitive") print $3; else print $2;}' | cut -d';' -f 1 )
  for reqexternal in $req; do
  echo $reqexternal
  external_module $reqexternal
  done
  for reqprod in $req; do
  echo $reqprod
  check_if_requires_external_module $reqprod
  javac -d cclasses --module-path $product $(find src/$reqprod -name "*.java") src/$reqprod/module-info.java
  jar --create --file $product/$reqprod.jar -C cclasses .
  rm -r cclasses
  done
  build_product
}

function check_if_requires_external_module() {
  targetpath="src/$1/module-info.java"
  req=$(cat $targetpath | grep "requires"| awk '{if ($2=="transitive") print $3; else print $2;}' | cut -d';' -f 1 )
  for reqexternal in $req; do
  echo $reqexternal
  external_module $reqexternal
  done
}

product=$1
mainclass=$2
echo $product
echo $mainclass
if [ -d "$1" ]; then rm -r $1; fi
if [ -d "classes" ]; then rm -r classes; fi 
mkdir $1
build_product_requirement $product