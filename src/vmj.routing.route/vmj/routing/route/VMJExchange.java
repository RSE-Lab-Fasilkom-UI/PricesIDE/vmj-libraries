package vmj.routing.route;
import java.util.*;
import com.sun.net.httpserver.HttpExchange;
import java.net.URLDecoder;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public class VMJExchange {
    private HttpExchange exchange;
    private String postBodyString;

    public VMJExchange(HttpExchange exchange) {
        this.exchange = exchange;
        this.postBodyString = getStringFromInputStream(exchange.getRequestBody());
    }

    public Object getPOSTBodyForm(String key) {
        HashMap<String,Object> inputMap = getInputAsDict(this.postBodyString);
        if (!inputMap.containsKey(key)) {
            return null;
        }
        return inputMap.get(key);
    }

    public String getGETParam(String key) {
        Map<String,String> inputParamsMap = queryToMap();
        if (!inputParamsMap.containsKey(key)) {
            return null;
        }
        return inputParamsMap.get(key);
    }

    public String getStringFromInputStream(InputStream inputStream) {
        Scanner sc = new Scanner(inputStream);
        StringBuffer sb = new StringBuffer();
        while(sc.hasNext()){
            try {
                sb.append(URLDecoder.decode(sc.nextLine(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        System.out.println(sb.toString());
        return sb.toString();
    }

    public HashMap<String,Object> getInputAsDict(String toBeSplitted) {
        String[] splitted = toBeSplitted.split("&");
        HashMap<String,Object> mapInput = new HashMap<>();
        for (String string : splitted) {
            String[] splitStrings = string.split("=");
            mapInput.put(splitStrings[0], splitStrings[1]);
        }
        return mapInput;
    }


    /**
     * source : https://stackoverflow.com/questions/11640025/how-to-obtain-the-query-string-in-a-get-with-java-httpserver-httpexchange
     */
    public Map<String, String> queryToMap() {
        String query = this.exchange.getRequestURI().getQuery();
        Map<String, String> result = new HashMap<>();
        for (String param : query.split("&")) {
            String[] entry = param.split("=");
            if (entry.length > 1) {
                result.put(entry[0], entry[1]);
            }else{
                result.put(entry[0], "");
            }
        }
        return result;
    }
}