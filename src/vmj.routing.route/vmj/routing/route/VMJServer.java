package vmj.routing.route;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpHandler;

import com.google.gson.*;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Arrays;

import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.util.Scanner;

import vmj.object.mapper.VMJDatabaseUtil;

public class VMJServer {
    private VMJDatabaseUtil vmjDBUtil;
    private HttpServer server = null;
    private static VMJServer vmjServerInstance = null;
    private String hostName;
    private int serverPort;

    private VMJServer(String hostName, int serverPort) {
        vmjDBUtil = new VMJDatabaseUtil();
        try {
            server = HttpServer.create(new InetSocketAddress(hostName, serverPort), 0);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        this.hostName = hostName;
        this.serverPort = serverPort;
    }

    public static VMJServer getInstance(String hostName, int serverPort) {
        if (vmjServerInstance == null) {
            vmjServerInstance = new VMJServer(hostName, serverPort);
        }
        return vmjServerInstance;
    }

    public static VMJServer getInstance() {
        if (vmjServerInstance == null) {
            return null;
        }
        return vmjServerInstance;
    }

    public void startServerGeneric() throws IOException {
        // server.createContext("/api/hello", (exchange -> {
        // String respText = "Hello!";
        // exchange.getResponseHeaders().set("Content-Type", "application/json");
        // exchange.sendResponseHeaders(200, respText.getBytes().length);
        // OutputStream output = exchange.getResponseBody();
        // output.write(respText.getBytes());
        // output.flush();
        // exchange.close();
        // }));

        // System.out.println("http://" + hostName + ":" + serverPort + "/api/hello");

        server.setExecutor(null); // creates a default executor
        server.start();
    }

    public void createURL(String endpoint, Object object, Method method) {
        server.createContext(("/" + endpoint), new HttpHandler() {
            @Override
            public void handle(final HttpExchange exchange) throws IOException {
                try {
                    exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
                    VMJExchange vmjExchange = new VMJExchange(exchange);

                    if (method.getReturnType().equals("void")) {
                        method.invoke(object, vmjExchange);
                    } else {
                        Object hasil = method.invoke(object, vmjExchange);

                        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
                        String jsonOutput = gson.toJson(hasil);

                        sendSuccessResponse(exchange, jsonOutput);
                    }

                } catch (Exception e) {

                }
            }
        });
        System.out.println("http://" + hostName + ":" + serverPort + "/" + endpoint);
    }

    public void sendSuccessResponse(HttpExchange exchange, String message) throws IOException {
        exchange.getResponseHeaders().set("Content-Type", "application/json");
        exchange.sendResponseHeaders(200, message.getBytes().length);
        OutputStream output = exchange.getResponseBody();
        output.write(message.getBytes());
        output.flush();
        exchange.close();
    }

    public void createTableCRUDEndpoint(String theEndpoint, String tableName, ArrayList<String> tableColumns) {
        server.createContext(("/crud/" + theEndpoint), new HttpHandler() {
            @Override
            public void handle(final HttpExchange exchange) throws IOException {
                exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
                String uriPath = exchange.getRequestURI().getPath();
                String[] uriParts = Arrays.copyOfRange(uriPath.split("/"), 1, uriPath.split("/").length);
                for (String string : uriParts) {
                    System.out.println(string);
                }
                if (uriParts.length == 2) {
                    /**
                     * POST new data
                     */
                    if (exchange.getRequestMethod().equals("POST")) {
                        String respText = "Inserting a data...";
                        String getString = getStringFromInputStream(exchange.getRequestBody());
                        System.out.println(getString);
                        HashMap<String, Object> hashInput = getInputAsDict(getString);
                        vmjDBUtil.insertDataToATable(tableName, hashInput);

                        sendSuccessResponse(exchange, respText);
                    } else if (exchange.getRequestMethod().equals("GET")) {
                        /**
                         * all datas in a table
                         */
                        String respText = "Querying all datas...";
                        String allQueryString = vmjDBUtil.createSelectAllQueryFromATable(tableName);

                        List<HashMap<String, Object>> queriedList = vmjDBUtil.hitDatabaseForQueryATable(allQueryString,
                                tableColumns);

                        Gson gson = new Gson();
                        respText = gson.toJson(queriedList);

                        sendSuccessResponse(exchange, respText);
                    }
                } else if (uriParts.length == 3) {
                    /**
                     * based on ID
                     */
                    if (exchange.getRequestMethod().equals("PUT")) {
                        /**
                         * UPDATE
                         */
                        String respText = "Updating a data...";
                        String getString = getStringFromInputStream(exchange.getRequestBody());
                        System.out.println(getString);
                        HashMap<String, Object> hashInput = getInputAsDict(getString);
                        vmjDBUtil.updateDataById(tableName, uriParts[2], hashInput);
                        respText = "berhasil update data";
                        sendSuccessResponse(exchange, respText);
                    } else if (exchange.getRequestMethod().equals("DELETE")) {
                        /**
                         * DELETE
                         */
                        String respText = "Deleting a data...";
                        vmjDBUtil.deleteRowById(tableName, uriParts[2]);
                        respText = "berhasil hapus data";
                        sendSuccessResponse(exchange, respText);
                    } else {
                        /**
                         * GET
                         */
                        String respText = "Querying a data...";

                        HashMap<String, Object> queriedData = vmjDBUtil.getDataById(tableName, tableColumns,
                                uriParts[2]);

                        Gson gson = new Gson();
                        respText = gson.toJson(queriedData);

                        sendSuccessResponse(exchange, respText);
                    }
                }
            }

        });

        System.out.println("http://" + hostName + ":" + serverPort + "/crud/" + theEndpoint);
    }

    public String getStringFromInputStream(InputStream inputStream) {
        Scanner sc = new Scanner(inputStream);
        StringBuffer sb = new StringBuffer();
        while (sc.hasNext()) {
            try {
                sb.append(URLDecoder.decode(sc.nextLine(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public HashMap<String, Object> getInputAsDict(String toBeSplitted) {
        String[] splitted = toBeSplitted.split("&");
        HashMap<String, Object> mapInput = new HashMap<>();
        for (String string : splitted) {
            String[] splitStrings = string.split("=");
            mapInput.put(splitStrings[0], splitStrings[1]);
        }
        return mapInput;
    }

    public void createCRUDEndpoints(String theEndpoint, String tableName, String parentTableName,
            Boolean isStandAloneTable, ArrayList<String> tableColumns, ArrayList<String> parentTableColumns,
            String foreignKeyColumnName) {
        // System.out.println("http://localhost:8000" + theEndpoint);
        server.createContext(("/crud/" + theEndpoint), new HttpHandler() {
            @Override
            public void handle(final HttpExchange exchange) throws IOException {
                exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
                String uriPath = exchange.getRequestURI().getPath();
                String[] uriParts = Arrays.copyOfRange(uriPath.split("/"), 1, uriPath.split("/").length);
                for (String string : uriParts) {
                    System.out.println(string);
                }
                if (uriParts.length == 2) {
                    /**
                     * POST new data
                     */
                    if (exchange.getRequestMethod().equals("POST")) {
                        if (parentTableName != null) {
                            String respText = "Inserting a data...";
                            String getString = getStringFromInputStream(exchange.getRequestBody());
                            System.out.println(getString);
                            HashMap<String, Object> hashInput = getInputAsDict(getString);

                            /**
                             * for parent table
                             */
                            HashMap<String, Object> hashInputParentTable = new HashMap<>();
                            for (String parentTableColumn : parentTableColumns) {
                                hashInputParentTable.put(parentTableColumn, hashInput.get(parentTableColumn));
                            }
                            int parentId = vmjDBUtil.insertDataToATable(parentTableName, hashInputParentTable);

                            /**
                             * for the table
                             */
                            HashMap<String, Object> hashInputTable = new HashMap<>();
                            for (String tableColumn : tableColumns) {
                                hashInputTable.put(tableColumn, hashInput.get(tableColumn));
                            }
                            hashInputTable.put(foreignKeyColumnName, parentId);

                            vmjDBUtil.insertDataToATable(tableName, hashInputTable);

                            sendSuccessResponse(exchange, respText);

                        } else if (isStandAloneTable) {
                            String respText = "Inserting a data...";
                            String getString = getStringFromInputStream(exchange.getRequestBody());
                            System.out.println(getString);
                            HashMap<String, Object> hashInput = getInputAsDict(getString);
                            vmjDBUtil.insertDataToATable(tableName, hashInput);

                            sendSuccessResponse(exchange, respText);
                        } else {
                            String respText = "Belum terhandle";
                            System.out.println("belum terhandle");
                            sendSuccessResponse(exchange, respText);
                        }
                    }

                    else if (exchange.getRequestMethod().equals("GET")) {
                        /**
                         * all datas in a table
                         */
                        String respText = "Querying all datas...";
                        String allQueryString = vmjDBUtil.createSelectAllQueryFromATable(tableName);

                        List<HashMap<String, Object>> queriedList = vmjDBUtil.hitDatabaseForQueryATable(allQueryString,
                                tableColumns);

                        Gson gson = new Gson();
                        respText = gson.toJson(queriedList);

                        sendSuccessResponse(exchange, respText);
                    }

                } else if (uriParts.length == 3) {
                    /**
                     * based on ID
                     */
                    if (exchange.getRequestMethod().equals("PUT")) {
                        /**
                         * UPDATE
                         */
                        String respText = "Updating a data...";
                        String getString = getStringFromInputStream(exchange.getRequestBody());
                        System.out.println(getString);
                        HashMap<String, Object> hashInput = getInputAsDict(getString);
                        vmjDBUtil.updateDataById(tableName, uriParts[2], hashInput);
                        respText = "berhasil update data";
                        sendSuccessResponse(exchange, respText);
                    } else if (exchange.getRequestMethod().equals("DELETE")) {
                        /**
                         * DELETE
                         */
                        String respText = "Deleting a data...";
                        vmjDBUtil.deleteRowById(tableName, uriParts[2]);
                        respText = "berhasil hapus data";
                        sendSuccessResponse(exchange, respText);
                    } else {
                        /**
                         * GET
                         */
                        String respText = "Querying a data...";

                        HashMap<String, Object> queriedData = vmjDBUtil.getDataById(tableName, tableColumns, uriParts[2]);

                        Gson gson = new Gson();
                        respText = gson.toJson(queriedData);

                        sendSuccessResponse(exchange, respText);
                    }
                }
            }
        });

        System.out.println("http://" + hostName + ":" + serverPort + "/crud/" + theEndpoint);
    }
}