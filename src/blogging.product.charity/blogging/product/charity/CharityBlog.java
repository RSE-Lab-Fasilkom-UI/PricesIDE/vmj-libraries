package blogging.product.charity;

import blog.page.BlogFactory;
import blog.page.core.Blog;

import vmj.routing.route.Router;
import vmj.routing.route.VMJServer;

import java.io.IOException;

public class CharityBlog {
    public static void main(String[] args) {
        DatabaseUtil dbUtil = new DatabaseUtil();
        VMJServer vmjServer = VMJServer.getInstance(8000);

        /**
         * activate server
         */
        try {
            vmjServer.startServerGeneric();      
        } catch (IOException e) {
            e.printStackTrace();
        }


        Blog blogShare = BlogFactory.createBlog("blog.page.share.BlogImpl", BlogFactory.createBlog("blog.page.core.BlogImpl"));

        Blog blogComment = BlogFactory.createBlog("blog.page.comment.BlogImpl", BlogFactory.createBlog("blog.page.core.BlogImpl"));

        System.out.println("==== BLOG SHARE ====");
        Router.route(blogShare);

        System.out.println("==== BLOG COMMENT ====");
        Router.route(blogComment);
    }
}