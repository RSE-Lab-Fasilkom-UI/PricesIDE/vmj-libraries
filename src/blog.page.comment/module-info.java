module blog.page.comment {
    requires vmj.routing.route;
    exports blog.page.comment;
    requires blog.page.core;
    requires vmj.object.mapper;
}