package blog.page.comment;

import java.util.*;
import blog.page.core.BlogDecorator;
import blog.page.core.BlogComponent;

import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;

import java.lang.UnsupportedOperationException;

import vmj.object.mapper.VMJDatabaseField;
import vmj.object.mapper.VMJDatabaseTable;
import vmj.object.mapper.VMJDatabaseUtil;

@VMJDatabaseTable(tableName = "blog_page_comment")
public class BlogImpl extends BlogDecorator {
    VMJDatabaseUtil vmjDBUtil;

    public BlogComponent blogComponent;

    @VMJDatabaseField(primaryKey = true, isDelta = true)
    public int id;

    @VMJDatabaseField(isDelta = true)
    public int comment;

    public BlogImpl(BlogComponent blogComponent) {
        super(blogComponent);
        this.blogComponent = blogComponent;
        this.vmjDBUtil = new VMJDatabaseUtil();
    }

    @Route(url = "blog-delta-comment/add-comment")
    public HashMap<String, Object> addComments(VMJExchange vmjExchange) {
        Object postId = vmjExchange.getPOSTBodyForm("postid");
        Object comment = vmjExchange.getPOSTBodyForm("comment");

        String sqlCommand = "INSERT INTO blog_page_comment (comment, blog) VALUES ('" + comment + "','" + postId
                + "');";
        this.vmjDBUtil.hitDatabase(sqlCommand);

        HashMap<String, Object> hasil = new HashMap<>();
        hasil.put("status", "succeed");
        return hasil;
    }

    @Route(url = "blog-delta-comment/get-comments")
    public List<HashMap<String, Object>> getComments(VMJExchange vmjExchange) {
        String sqlCommand = "select * from blog_page_comment";
        ArrayList<String> reqFields = new ArrayList<>();
        reqFields.add("id");
        reqFields.add("comment");
        reqFields.add("blog");
        List<HashMap<String, Object>> hasil = this.vmjDBUtil.hitDatabaseForQueryATable(sqlCommand, reqFields);
        return hasil;
    }

    /**
     * remove endpoint by overriding the target method and not including the @Route
     * annotation
     */
    @Override
    public HashMap<String, Object> createPost(VMJExchange vmjExchange) {
        throw new UnsupportedOperationException();
    }
}