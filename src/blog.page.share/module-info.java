module blog.page.share {
    requires vmj.routing.route;
    requires blog.page.core;
    exports blog.page.share;
    requires vmj.object.mapper;
}