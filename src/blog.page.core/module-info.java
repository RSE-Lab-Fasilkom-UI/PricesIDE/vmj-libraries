module blog.page.core {
    requires vmj.routing.route;
    requires java.logging;
    exports blog.page.core;
    exports blog.page;
    requires vmj.object.mapper;
}
