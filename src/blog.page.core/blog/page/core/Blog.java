package blog.page.core;
import java.util.*;
import vmj.routing.route.VMJExchange;

public interface Blog {
    HashMap<String,Object> createPost(VMJExchange vmjExchange);
    List<HashMap<String,Object>> getPosts(VMJExchange vmjExchange);
    HashMap<String,Object> getPost(VMJExchange vmjExchange);
}