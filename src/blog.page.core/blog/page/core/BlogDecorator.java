package blog.page.core;
import vmj.object.mapper.VMJDatabaseField;

public abstract class BlogDecorator extends BlogComponent {
    
    @VMJDatabaseField(foreignTableName="blog_core_post", foreignColumnName = "id", isDelta=true)
    public BlogComponent blog = null;

    public BlogDecorator (BlogComponent blog) {
        this.blog = blog;
    }

}