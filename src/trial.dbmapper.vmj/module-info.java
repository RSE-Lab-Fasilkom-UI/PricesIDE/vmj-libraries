module trial.dbmapper.vmj {
    requires vmj.object.mapper;
    requires vmj.routing.route;
    requires blog.page.core;
    requires blog.page.comment;
    requires blog.page.share;
}