package trial.dbmapper.vmj;
import java.util.ArrayList;

import vmj.object.mapper.VMJDatabaseMapper;
import vmj.routing.route.VMJServer;

public class Main {
    
    public static void main(String[] args) {
        VMJServer vmjServer = VMJServer.getInstance("localhost", 8000);
        try {
            vmjServer.startServerGeneric();
        } catch (Exception e) {
            //TODO: handle exception
        }

        /**
         * create table
         */
        VMJDatabaseMapper.generateTable("blog.page.core.BlogImpl", false);
        VMJDatabaseMapper.generateTable("blog.page.comment.BlogImpl", true);
        VMJDatabaseMapper.generateTable("blog.page.share.BlogImpl", true);

        /**
         * generate CRUD endpoint
         */
        vmjServer.createTableCRUDEndpoint("posts", "blog_core_post", VMJDatabaseMapper.getTableColumnsNames("blog.page.core.BlogImpl", false));
        vmjServer.createTableCRUDEndpoint("comments", "blog_page_comment", VMJDatabaseMapper.getTableColumnsNames("blog.page.comment.BlogImpl", true));
        vmjServer.createTableCRUDEndpoint("shareinfos", "blog_page_share", VMJDatabaseMapper.getTableColumnsNames("blog.page.share.BlogImpl", true));


    }
}