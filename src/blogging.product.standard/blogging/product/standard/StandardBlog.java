package blogging.product.standard;

import blog.page.BlogFactory;
import blog.page.core.Blog;
import blog.page.comment.BlogImpl;

public class StandardBlog {
    public static void main(String[] args) {
        Blog standardBlog = BlogFactory.createBlog("blog.page.comment.BlogImpl", BlogFactory.createBlog("blog.page.core.Impl"));
    }
}