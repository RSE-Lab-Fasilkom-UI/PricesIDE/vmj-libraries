module blogging.product.standard {
    requires vmj.object.mapper;
    requires vmj.routing.route;

    requires transitive blog.page.core;
    requires transitive blog.page.comment;
    requires transitive blog.page.share;
}